/**
 * Created by Nicole on 1/25/16.
 */

//var express     = require('express');
var flash       = require('express-flash');
//var nodemailer  = require('nodemailer');
//var sgTransport = require('nodemailer-sendgrid-transport');
var config      = require('./config/config');

module.exports = function (app, express, nodemailer, sgTransport, flash) {

    // Route all Traffic to Secure Server
    // Order is important (this should be the first route)
    /*function requireHTTPS(req, res, next) {
        if(!req.secure) {
            return res.redirect('https://' + req.get('host') + req.url);
        }
        next();
    }
    app.use(requireHTTPS);*/

    /**
     * GET /
     * Render Home page
     */
    app.get('/', function (req, res) {
        res.render('index', {
            title: 'microNovations'
        });
    });

    /**
     * GET /services
     * Render services page
     */
    app.get('/services', function (req, res) {
        res.render('services', {
            title: 'microNovations | Services'
        });
    });

    /**
     * POST /
     * Handle index page contact form post request
     */
    app.post('/', function(req, res) {
        var sg = require('sendgrid')(process.env.SENDGRID_API_KEY);
        
        var request = sg.emptyRequest({
            method: 'POST',
            path: '/v3/mail/send',
            body: {
                from: {
                    email: req.body.email,
                    name: req.body.name
                },
                personalizations: [{
                    to: [{ email: 'mnobles@micronovations.com' }],                   
                }],
                subject: req.body.subject,
                content: [{
                    type: 'text/plain',
                    value: req.body.message
                }]
            }
        });

        sg.API(request, function( error, response ) {
            if (error) {
                res.render('index', {
                    msg: 'Error, message not sent ' + error,
                    err: true,
                    page: 'index#contact'
                });
            }
            else {
                res.render('index', {
                    msg: 'Message sent! Thank you.',
                    err: false,
                    page: 'index#contact'
                });
            }
            //req.flash('info', {msg: this.msg});
        });
    });
};
