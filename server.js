#!/bin/env node
/**
 * Created by Nicole on 1/19/2016.
 */

/**
 * Module Dependencies
 */

// Express 4.x.x Modules
var express         = require('express');
var http            = require('http');
var https           = require('https');
var flash           = require('express-flash');
var session         = require('express-session');
var path            = require('path');
var favicon         = require('serve-favicon');
var logger          = require('morgan');
var cookieParser    = require('cookie-parser');
var bodyParser      = require('body-parser');
var nodemailer      = require('nodemailer');
var sgTransport     = require('nodemailer-sendgrid-transport');

// Additional Modules
var fs              = require('fs');
var config          = require('./config/config');

/**
 * Create Express app
 */

var app = express();

/**
 * Express Configuration and Setup
 */

// https: SSL Key and Cert
var httpsOptions = {
    //key: fs.readFileSync('/path/to/key.pem'),
    //cert: fs.readFileSync('/path/to/cert.pem')
};

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));
app.use(cookieParser());
app.use(session({
    secret: 'keyboard cat',
    resave: false,
    saveUninitialized: true,
    cookie: { secure: true }
}));
app.use(flash());
app.use(require('less-middleware')(path.join(__dirname, 'public')));
app.use(express.static(path.join(__dirname, 'public')));
app.use('/client',  express.static(__dirname + '/client'));

/**
 * Routes/Routing
 */
require('./routes.js')(app, express, nodemailer, sgTransport);

/**
 * Error Handling
 */

// catch 404 and forward to error handler
app.use(function (req, res, next) {
    var err = new Error('Not Found');
    err.status = 404;
    next(err);
});

// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
    app.use(function (err, req, res, next) {
        res.status(err.status || 500);
        res.render('error', {
            message: err.message,
            error: err
        });
    });
}

// production error handler
// no stacktraces leaked to user
app.use(function (err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
        message: err.message,
        error: {}
    });
});

var host = process.env.HOST || '127.0.0.1';
var port = process.env.PORT || 3000;

//app.listen(port, ip);
app.listen(port, function() {
    console.log('Express server listening on %d, in %s mode', port, app.get('env'));
});

module.exports = app;



