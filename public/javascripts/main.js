/**
 * Created by Jan on 1/19/2016.
 */

$(document).ready(function() {


    /*-----------------------------------------------------------------------
        Owl Carousels
     -----------------------------------------------------------------------*/

    // HOME SLIDER ----------------------------------------------------------
    $("#homepage-main-carousel").owlCarousel({
        navigation : false,
        singleItem : true,
        lazyLoad: true,
        stopOnHover: true,
        autoHeight: false,

        autoPlay: 7000 // 7 seconds
    });

    // SOLUTIONS SLIDER -----------------------------------------------------
    $("#hosted_solutions").owlCarousel({
        navigation : false,
        pagination : false,
        singleItem : true,
        lazyLoad: true,

        autoPlay: 5000 // 5 seconds
    });

    // PARTNERS SLIDER ------------------------------------------------------
    $("#partners").owlCarousel({
        autoPlay: 3000, //3 seconds
        items : 6,
        itemsDesktopSmall: [979,6],
        itemsTablet: [600,4],
        itemsMobile: [479,3]
    });
    // TESTIMONIAL SLIDER ---------------------------------------------------
    $("#testimonials").owlCarousel({
        navigation : false,
        singleItem : true,
        lazyLoad: true,

        autoPlay: 15000 // 15 seconds
    });
    // CLIENTS SLIDER -------------------------------------------------------
    $("#clients").owlCarousel({
        autoPlay: 3000, //3 seconds
        items : 4,
        itemsDesktopSmall: [979,4],
        itemsTablet: [600,4],
        itemsMobile: [479,3]
    });
    // WEB_BANNER SLIDER ----------------------------------------------------
    $("#ms-banner").owlCarousel({
        navigation : false,
        pagination : true,
        singleItem : true,
        stopOnHover: true,
        lazyLoad: true,

        autoPlay: 5000 //5 seconds
    });

    // BACK TO TOP ----------------------------------------------------------
    // fade in #backToTop
    //$('.back-to-top').css({'display':'none'});

    $(window).scroll(function () {
        if ($(this).scrollTop() > 250) {
            $('.back-to-top').fadeIn(300);
        }
        else {
            $('.back-to-top').fadeOut(300);
        }
    });

    $('.back-to-top').click(function(event){
        event.preventDefault();
        $('html, body').animate({scrollTop: 0}, 400);
        return false;
    });

    /*--------- accordion ---------*/
    $('.collapse').on('shown.bs.collapse', function(){
        $(this).parent().find(".fa-plus").removeClass("fa-plus").addClass("fa-minus");
    }).on('hidden.bs.collapse', function(){
        $(this).parent().find(".fa-minus").removeClass("fa-minus").addClass("fa-plus");
    });
});
